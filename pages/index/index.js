//index.js
//获取应用实例
import { Index } from './index-model.js'
var home = new Index();
const app = getApp()
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    TabCur: 0,
    titleInfo: 'all',
    scrollLeft: 0,
    types: ['all','Android','iOS','休息视频','拓展资源','前端'],
    DataArr: []
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      titleInfo: this.data.types[e.currentTarget.dataset.id],
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
    this._loadData();
  },
  //滑动切换同步导航菜单
  swiperTab(e){
    this.setData({
      TabCur: e.detail.current,
      titleInfo: this.data.types[e.detail.current],
      scrollLeft: (e.detail.current - 1) * 60
    })
    this._loadData();
  },
  //初始化加载一次
  onLoad: function () {
    //获取all数据
    this._loadData();
  },
  //加载数据 Todo
  _loadData:function(){
    home.getDataList(this.data.titleInfo,(res)=>{
      console.log(res)
      this.setData({
        DataArr: res.data.results
      })
    })
  }
})
