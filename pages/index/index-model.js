import { Base } from '../../utils/base.js'

class Index extends Base {
  constructor(){
    super()
  }
  
  getDataList(typeName,callBack){
    var params = {
      'url': '/data/'+ typeName +'/10/1',
      sCallBack: function(res){
        callBack && callBack(res);
      }
    }
    this.request(params);
  }
}

export {Index}